// 1. Getting the cube of a number
	const cube = (number) => {
		const getCube = number ** 3;
		console.log(`The cube of ${number} is ${getCube}.`);
	};

	cube(2);

// 2. Printing address array
	let address = (location, country, countryCode) => {
		console.log(`I live at ${location}, ${country} ${countryCode}`);
	};
	address('258 Washington Ave NW', 'California', 90011);

// 3. Lolong crocodile
	let crocodile = {
		name: 'Lolong',
		species: 'saltwater crocodile',
		weight: 1075,
		measurement: '20 ft 3 in'
	};

	console.log(`${crocodile.name} was a ${crocodile.species}. He weighted at ${crocodile.weight} kgs with a measurement of ${crocodile.measurement}.`);

// 4. Array of numbers
	let numbers = [1,2,3,4,5];

	 numbers.forEach((number) => {
	 	console.log(number);
	});

	let number = numbers.reduce((x,y) => x+y )
	console.log(number);

// 5. Creating class Dog
	class Dog {
		constructor(name, age, breed){
			this.name = name;
			this.age = age;
			this.breed = breed;
		}
	};

	let myDog = new Dog('Frankie', 5, 'Miniature Dachshund');
	console.log(myDog);